//
//  PersonTests.m
//  TraqRuntime
//
//  Created by Samuelsson, Markus () on 2015-03-04.
//  Copyright (c) 2015 Samuelsson, Markus (). All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Person.h"


@interface PersonTests : XCTestCase

@end


@implementation PersonTests
{
    Person *person;
}

- (void)setUp
{
    [super setUp];
    person = [[Person alloc] init];
}

-(void)testNameIsNil
{
    XCTAssertNil(person.name);
}

-(void)testSetName
{
    XCTAssertNoThrow(person.name = @"Kalle");
}

-(void)testGetName
{
    XCTAssertNoThrow(person.name);
}

-(void)testSetAndGetName
{
    NSString *expectedName = @"Robert";
    person.name = expectedName;
    XCTAssertEqualObjects(expectedName, person.name);
}

-(void)testDataSavedOnInstance
{
    Person *otherPerson = [[Person alloc] init];
    person.name = @"Robert";
    XCTAssertNil(otherPerson.name);
}


-(void)testSetAndGetNickname
{
    NSString *expectedNickName = @"Robban";
    [person setNickName:expectedNickName];
    XCTAssertEqualObjects(expectedNickName, [person nickName]);
}

// Test if the implemtation can handle a "property" that is not declared
-(void)testSetAndGetUndeclaredProperty
{
    
    NSString *expectedAddress = @"Vasagatan 2";
    [person performSelector:NSSelectorFromString(@"setAddress:") withObject:expectedAddress];
    NSString *actualAddress = [person performSelector:NSSelectorFromString(@"address") withObject:nil];
    XCTAssertEqualObjects(expectedAddress, actualAddress);
}


@end
