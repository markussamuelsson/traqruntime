//
//  UppercaseTests.m
//  TraqRuntime
//
//  Created by Samuelsson, Markus () on 2015-03-04.
//  Copyright (c) 2015 Samuelsson, Markus (). All rights reserved.
//

#import <XCTest/XCTest.h>
#import <objc/runtime.h>

@interface UppercaseTests : XCTestCase
@end

@implementation UppercaseTests

-(void)setUp
{
    //perform method swizzling here
}

-(void)testMethodSwizzling
{
    NSString *strWithMixedCase = @"KallE";
    XCTAssertEqualObjects(@"kalle", [strWithMixedCase uppercaseString]);
    XCTAssertEqualObjects(@"KALLE", [strWithMixedCase lowercaseString]);
    
}

@end
