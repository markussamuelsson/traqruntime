//
//  DetailViewController.m
//  TraqRuntime
//
//  Created by Samuelsson, Markus () on 2015-02-26.
//  Copyright (c) 2015 Samuelsson, Markus (). All rights reserved.
//

#import "DetailViewController.h"
#import "SensitiveOperationManager.h"
#import "Bat.h"

@interface DetailViewController ()

- (IBAction)pressedSensitiveOperation:(id)sender;
- (IBAction)pressedBat:(id)sender;

@end

@implementation DetailViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

//Makes calls to methods in SensitiveOperationManager
- (IBAction)pressedSensitiveOperation:(id)sender
{    
    NSLog(@"%s", __FUNCTION__);
    SensitiveOperationManager *manager = [[SensitiveOperationManager alloc] init];
    [manager op1];
    [manager op2];
    [manager op3];
    [manager op4WithJSON:[NSDictionary dictionary]];
    [manager op5];
    [manager op6];
    [manager op7];
    
    NSData *data = [@"test" dataUsingEncoding:NSUTF8StringEncoding];
    [manager op8WithData:data];
    [manager op9];
}

//Make the bat flap and breath.
- (IBAction)pressedBat:(id)sender
{
    Bat *bat = [[Bat alloc] init];
    [bat flap];
    [bat breathe];
}



@end
