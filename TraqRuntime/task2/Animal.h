//
//  Animal.h
//  TraqRuntime
//
//  Created by Samuelsson, Markus () on 2015-02-27.
//  Copyright (c) 2015 Samuelsson, Markus (). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Animal : NSObject
@end

@protocol Mammal <NSObject>
-(void)breathe;
@end


@interface MammalImpl : Animal<Mammal>
-(void)breathe;
@end


@protocol WingedAnimal <NSObject>
-(void)flap;
@end


@interface WingedAnimalImpl : Animal<WingedAnimal>

-(void)flap;

@end

