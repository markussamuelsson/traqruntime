//
//  Bat.h
//  TraqRuntime
//
//  Created by Samuelsson, Markus () on 2015-02-27.
//  Copyright (c) 2015 Samuelsson, Markus (). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Animal.h"

// A bat is a winged mammal
// How should it be implemented?
@interface Bat : NSObject<Mammal, WingedAnimal>


@end
