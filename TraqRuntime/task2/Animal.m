//
//  Animal.m
//  TraqRuntime
//
//  Created by Samuelsson, Markus () on 2015-02-27.
//  Copyright (c) 2015 Samuelsson, Markus (). All rights reserved.
//

#import "Animal.h"

@implementation Animal

@end

@implementation MammalImpl

-(void)breathe
{
    //Some really complicated breath code that we do not want to duplicate...
    NSLog(@"breathing...");
}

@end

@implementation WingedAnimalImpl

-(void)flap
{
    //Some really complicated flap code that we do not want to duplicate...
    NSLog(@"flapping...");
}

@end