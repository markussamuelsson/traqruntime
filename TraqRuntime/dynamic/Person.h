//
//  TraqRuntime
//
//  Created by Samuelsson, Markus () on 2015-03-03.
//  Copyright (c) 2015 Samuelsson, Markus (). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *nickName;

@end
