//
//  SensitiveOperationManager.m
//  TraqRuntime
//
//  Created by Samuelsson, Markus () on 2015-02-27.
//  Copyright (c) 2015 Samuelsson, Markus (). All rights reserved.
//

#import "SensitiveOperationManager.h"

@implementation SensitiveOperationManager

-(void)op1
{
    //implementation
}

-(void)op2
{
    //implementation
}

-(void)op3
{
    //implementation
}

-(void)op4WithJSON:(NSDictionary *)json
{
    //implementation
}

-(void)op5
{
    //implementation
}

-(void)op6
{
    //implementation
}

-(void)op7
{
    //implementation
}

-(void)op8WithData:(NSData *)data
{
    //implementation
}

-(void)op9
{
    //implementation
}




@end
