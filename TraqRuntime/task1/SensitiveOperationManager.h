//
//  SensitiveOperationManager.h
//  TraqRuntime
//
//  Created by Samuelsson, Markus () on 2015-02-27.
//  Copyright (c) 2015 Samuelsson, Markus (). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SensitiveOperationManager : NSObject

-(void)op1;
-(void)op2;
-(void)op3;
-(void)op4WithJSON:(NSDictionary *)json;
-(void)op5;
-(void)op6;
-(void)op7;
-(void)op8WithData:(NSData *)data;
-(void)op9;

@end
