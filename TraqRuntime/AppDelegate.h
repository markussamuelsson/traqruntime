//
//  AppDelegate.h
//  TraqRuntime
//
//  Created by Samuelsson, Markus () on 2015-02-26.
//  Copyright (c) 2015 Samuelsson, Markus (). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

