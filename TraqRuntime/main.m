//
//  main.m
//  TraqRuntime
//
//  Created by Samuelsson, Markus () on 2015-02-26.
//  Copyright (c) 2015 Samuelsson, Markus (). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
